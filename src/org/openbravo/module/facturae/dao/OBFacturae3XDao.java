/************************************************************************************ 
 * Copyright (C) 2015 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************/

package org.openbravo.module.facturae.dao;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.common.invoice.InvoiceTax;
import org.openbravo.model.financialmgmt.tax.TaxRate;

/**
 * This class contains the specific code used by the OBFacturae3X implementation classes to access
 * the database using DAL (Hibernate)
 * 
 * @author openbravo
 * 
 */
public class OBFacturae3XDao {

  /**
   * Returns true if the given invoice has taxes
   * 
   * @param obInvoice
   *          Openbravo's invoice
   * @return true if the given invoice has taxes
   */
  public boolean isTaxIncluded(Invoice obInvoice) {
    List<InvoiceTax> invoiceTaxLines = obInvoice.getInvoiceTaxList();
    if (invoiceTaxLines.size() > 0)
      return true;
    else
      return false;
  }

  /**
   * Returns the set of tax rates included in the given invoice
   * 
   * @param obInvoice
   *          Openbravo's invoice
   * @return the set of tax rates included in the given invoice
   */
  public Set<TaxRate> getTaxes(Invoice obInvoice) {
    try {
      OBContext.setAdminMode(true);
      final StringBuilder whereClause = new StringBuilder();

      whereClause.append(" as t , InvoiceTax as it ");
      whereClause.append(" where it.tax.id = t.id ");
      whereClause.append(" and it.invoice.id='" + obInvoice.getId() + "'");

      final OBQuery<TaxRate> obQuery = OBDal.getInstance().createQuery(TaxRate.class,
          whereClause.toString());
      return new HashSet<TaxRate>(obQuery.list());
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  /**
   * Returns true if the invoice has withholding taxes
   * 
   * @param obInvoice
   *          Openbravo's invoice
   * @return true if the invoice has withholding taxes
   */
  public boolean hasWithholdingTax(Invoice obInvoice) {
    Set<TaxRate> taxes = this.getTaxes(obInvoice);
    for (TaxRate tax : taxes) {
      if (tax.isWithholdingTax())
        return true;
    }
    return false;
  }

  /**
   * Returns true if the invoice has equivalent charge taxes
   * 
   * @param obInvoice
   *          Openbravo's invoice
   * @return true if the invoice has equivalent charge taxes
   */
  public boolean hasEquivalentChargeTax(Invoice obInvoice) {
    Set<TaxRate> taxes = this.getTaxes(obInvoice);
    for (TaxRate tax : taxes) {
      if (tax.isOBSPTIEquivalentCharge())
        return true;
    }
    return false;
  }

  /**
   * Returns the equivalent-charge tax rate associated with the given parent tax rate
   * 
   * @param parentTax
   *          Openbravo's tax rate. It must be a summary level tax rate
   * @return the associated equivalent-charge tax rate or null in case no equivalent charge tax rate
   *         found
   */
  public TaxRate getChildEquivalentCharge(TaxRate parentTax) {
    try {
      OBContext.setAdminMode(true);
      final StringBuilder whereClause = new StringBuilder();

      whereClause.append(" as c ");
      whereClause.append(" where c.parentTaxRate = '" + parentTax.getId() + "' ");
      whereClause.append(" and c.oBSPTIEquivalentCharge='Y' ");

      final OBQuery<TaxRate> obQuery = OBDal.getInstance().createQuery(TaxRate.class,
          whereClause.toString());
      List<TaxRate> taxList = obQuery.list();
      if (taxList.isEmpty())
        return null;
      else
        return taxList.get(0);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  /**
   * Returns the child tax rates of the given parentTax rate. If no child taxes or
   * includeParent=true, the given parentTax will also be included
   * 
   * @param parentTax
   *          Openbravo's Tax Rate
   * @param includeParent
   *          if true include also the parentTax in the list
   * @return a list of all the child tax rates of the given parentTax
   */
  public List<TaxRate> getChildTaxRates(TaxRate parentTax, boolean includeParent) {
    try {
      OBContext.setAdminMode(true);
      final StringBuilder whereClause = new StringBuilder();

      whereClause.append(" as cht , FinancialMgmtTaxRate as pt ");
      whereClause.append(" where cht.parentTaxRate.id = pt.id ");
      whereClause.append(" and pt.id='" + parentTax.getId() + "'");

      final OBQuery<TaxRate> obQuery = OBDal.getInstance().createQuery(TaxRate.class,
          whereClause.toString());
      List<TaxRate> taxList = obQuery.list();

      if (taxList.isEmpty() || includeParent) {
        taxList.add(parentTax);
      }
      return taxList;
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  /**
   * Returns the withholding tax rate associated with the given parent tax rate
   * 
   * @param parentTax
   *          Openbravo's tax rate. It must be a summary level tax rate
   * @return the associated withholding tax rate or null in case no equivalent charge tax rate found
   */
  public TaxRate getChildWithholding(TaxRate parentTax) {
    try {
      OBContext.setAdminMode(true);
      final StringBuilder whereClause = new StringBuilder();

      whereClause.append(" as c ");
      whereClause.append(" where c.parentTaxRate = '" + parentTax.getId() + "' ");
      whereClause.append(" and c.withholdingTax='Y' ");

      final OBQuery<TaxRate> obQuery = OBDal.getInstance().createQuery(TaxRate.class,
          whereClause.toString());
      List<TaxRate> taxList = obQuery.list();
      if (taxList.isEmpty())
        return null;
      else
        return taxList.get(0);
    } finally {
      OBContext.restorePreviousMode();
    }
  }
}
